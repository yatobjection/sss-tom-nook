const Discord = require('discord.js');
const client = new Discord.Client();
const auth = require('./auth.json');
const mysql = require('mysql');
const mysqldata = require ('./mySqlClient.json')

var mySqlClient = mysql.createConnection({
  host: mysqldata.host,
  port: mysqldata.post,
  user: mysqldata.user,
  password: mysqldata.password,
  database: mysqldata.database
});

var gainPerMsg = 0.1;
var moneyWhenCreatingAccount = 5;

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', (receivedMessage) => {
    if (receivedMessage.author == client.user) { // Prevent bot from responding to its own messages
        return
    }

    if (receivedMessage.content.startsWith("t!") && (receivedMessage.author.id == '300753414160384004')) {
    //if (receivedMessage.content.startsWith("t!") && (receivedMessage.channel.id == '540164481653800979')) {
        processCommand(receivedMessage)
    }
    else {
        earnMoneyPerMsg(receivedMessage)
    }
})

function processCommand(receivedMessage) {
    let fullCommand = receivedMessage.content.substr(2) // Remove the leading exclamation mark
    let splitCommand = fullCommand.split(" ") // Split the message up in to pieces for each space
    let primaryCommand = splitCommand[0] // The first word directly after the exclamation is the command
    let arguments = splitCommand.slice(1) // All other words are arguments/parameters/options for the command

    let admin = ["300753414160384004"];
    let bartender = ["300753414160384004", "373930793598320640"];
    // Private
    let bda = "540164481653800979";

    // Le vrai BDA
    // let bda = "554278583078748190";
    let taverneid = "533404481241743361";

    console.log("Command received: " + primaryCommand)
    console.log("Arguments: " + arguments) // There may not be any arguments

    // ==================== PRINCIPAL ==================== //
    if ((primaryCommand == "aide" || primaryCommand == "help") && receivedMessage.channel.id == bda) {
        helpCommand(arguments, receivedMessage, bartender)
    } else if (primaryCommand == "id" && admin.includes(receivedMessage.author.id)) {
        getIdCommand(arguments, receivedMessage)
    } else if (primaryCommand == "channelid" && admin.includes(receivedMessage.author.id)) {
        getChannelIdCommand(arguments, receivedMessage)
    } else if ((primaryCommand == "solde" || primaryCommand == "balance") && receivedMessage.channel.id == bda) {
        testBalance(arguments, receivedMessage, admin, moneyWhenCreatingAccount)
    } else if ((primaryCommand == "payer" || primaryCommand == "pay") && (receivedMessage.channel.id == bda || receivedMessage.channel.id == taverneid)) {
        payCommand(arguments, receivedMessage)
    } else if (primaryCommand == "caisse" && bartender.includes(receivedMessage.author.id) && receivedMessage.channel.id == taverneid) {
        caisseCommand(arguments, receivedMessage)
    } else if (primaryCommand == "bank" && admin.includes(receivedMessage.author.id)) {
        bankCommand(arguments, receivedMessage)
    } else if (primaryCommand == "taxes" && admin.includes(receivedMessage.author.id)) {
        taxesCommand(arguments, receivedMessage)
    } else if (primaryCommand == "hardreset" && admin.includes(receivedMessage.author.id)) {
        hardReset(arguments, receivedMessage)
    } else if (primaryCommand == "setmaxvalue" && admin.includes(receivedMessage.author.id)) {
        setMaxEconomyValue(arguments, receivedMessage)
    } else if (primaryCommand == "checkOutdated" && admin.includes(receivedMessage.author.id)) {
        checkOutdated(arguments, receivedMessage)
    }

    // ==================== MENU ==================== //
    else if (primaryCommand == "menu") {
        menuCommand(arguments, receivedMessage)
    } else if (primaryCommand == "menuHD" || primaryCommand == "menuhd") {
        menuHDCommand(arguments, receivedMessage)
    }
    // ---------- Wraps ---------- //
    else if (primaryCommand == "classsic" && bartender.includes(receivedMessage.author.id)) {
        menuWrapsClasssic(arguments, receivedMessage)
    } else if ((primaryCommand == "ssspécial" || primaryCommand == "ssspecial") && bartender.includes(receivedMessage.author.id)) {
        menuWrapsSSSpecial(arguments, receivedMessage)
    }
    // ---------- Burgers ---------- //
    else if ((primaryCommand == "cheessseburger" || primaryCommand == "cburger") && bartender.includes(receivedMessage.author.id)) {
        menuBurgersCheessseburger(arguments, receivedMessage)
    } else if ((primaryCommand == "sssburger" || primaryCommand == "sburger") && bartender.includes(receivedMessage.author.id)) {
        menuBurgersSSSBurger(arguments, receivedMessage)
    }
    // ---------- Plats ---------- //
    else if (primaryCommand == "pates" && bartender.includes(receivedMessage.author.id)) {
        menuDishesPates(arguments, receivedMessage)
    }
    // ---------- Boissons ---------- //
    else if (primaryCommand == "eau" && bartender.includes(receivedMessage.author.id)) {
        menuDrinkEau(arguments, receivedMessage)
    } else if ((primaryCommand == "ssscola" || primaryCommand == "cola") && bartender.includes(receivedMessage.author.id)) {
        menuDrinkSSSCola(arguments, receivedMessage)
    } else if ((primaryCommand == "isssetea" || primaryCommand == "tea") && bartender.includes(receivedMessage.author.id)) {
        menuDrinkIssseTea(arguments, receivedMessage)
    } else if ((primaryCommand == "sssmoothie" || primaryCommand == "smoothie") && bartender.includes(receivedMessage.author.id)) {
        menuDrinkSSSmoothie(arguments, receivedMessage)
    } else if (primaryCommand == "kanaille" && bartender.includes(receivedMessage.author.id)) {
        menuDrinkKanaille(arguments, receivedMessage)
    }
    // ---------- Bières ---------- //
    else if ((primaryCommand == "desperados" || primaryCommand == "despe") && bartender.includes(receivedMessage.author.id)) {
        menuBeerDesperados(arguments, receivedMessage)
    } else if (primaryCommand == "heineken" && bartender.includes(receivedMessage.author.id)) {
        menuBeerHeineken(arguments, receivedMessage)
    } else if (primaryCommand == "asahi" && bartender.includes(receivedMessage.author.id)) {
        menuBeerAsahi(arguments, receivedMessage)
    } else if ((primaryCommand == "affligem" || primaryCommand == "affli") && bartender.includes(receivedMessage.author.id)) {
        menuBeerAffligem(arguments, receivedMessage)
    } else if ((primaryCommand == "grimbergen" || primaryCommand == "grim") && bartender.includes(receivedMessage.author.id)) {
        menuBeerGrimbergen(arguments, receivedMessage)
    } else if (primaryCommand == "brooklyn" && bartender.includes(receivedMessage.author.id)) {
        menuBeerBrooklyn(arguments, receivedMessage)
    }
    // ---------- Cafés ---------- //
    else if ((primaryCommand == "café" || primaryCommand == "cafe") && bartender.includes(receivedMessage.author.id)) {
        menuCoffeeCafe(arguments, receivedMessage)
    } else if ((primaryCommand == "grandcafé" || primaryCommand == "grandcafe") && bartender.includes(receivedMessage.author.id)) {
        menuCoffeeGrandCafe(arguments, receivedMessage)
    } else if (primaryCommand == "latte" && bartender.includes(receivedMessage.author.id)) {
        menuCoffeeLatte(arguments, receivedMessage)
    } else if (primaryCommand == "grandlatte" && bartender.includes(receivedMessage.author.id)) {
        menuCoffeeGrandLatte(arguments, receivedMessage)
    } else if (primaryCommand == "grandlatte" && bartender.includes(receivedMessage.author.id)) {
        menuCoffeeCappuccino(arguments, receivedMessage)
    } else if ((primaryCommand == "cafégourmand" || primaryCommand == "cafegourmand") && bartender.includes(receivedMessage.author.id)) {
        menuCoffeeCafeGourmand(arguments, receivedMessage)
    }
    // ---------- Desserts ---------- //
    else if (primaryCommand == "cookies" && bartender.includes(receivedMessage.author.id)) {
        menuDessertCookies(arguments, receivedMessage)
    } else if ((primaryCommand == "bananasssplit" || primaryCommand == "bs") && bartender.includes(receivedMessage.author.id)) {
        menuDessertBananaSSSplit(arguments, receivedMessage)
    } else if (primaryCommand == "tiramisssu" && bartender.includes(receivedMessage.author.id)) {
        menuDessertTiramisssu(arguments, receivedMessage)
    }

    // ==================== AUTRE ==================== //
    else {
        receivedMessage.channel.send("Je n'ai pas compris la commande. Essayez `t!aide`")
    }
}

// ==================== PRINCIPAL ==================== //
function helpCommand(arguments, receivedMessage, bartender) {
    if (bartender.includes(receivedMessage.author.id)) {
        receivedMessage.author.send("**__Liste des commandes :__**\n\nt!solde : voir le solde de votre compte.\nt!payer [@Utilisateur] [Montant] : donner une somme à la personne choisie.\nt!menu : voir le menu de la taverne.\n\n**__Liste des commandes du bar :__**\n\nt!caisse [Montant] : Mettre de l'argent dans la caisse de la taverne.");
        receivedMessage.react("✅");
    } else {
        receivedMessage.author.send("**__Liste des commandes :__**\n\nt!solde : voir le solde de votre compte.\nt!payer [@Utilisateur] [Montant] : donner une somme à la personne choisie.\nt!menu : voir le menu de la taverne.");
        receivedMessage.react("✅");
    }
}

function getIdCommand(arguments, receivedMessage) {
    var userId = arguments.toString().split("@")[1].split(">")[0];
    userId = userId.startsWith("!") ? userId.substring(1) : userId;
    receivedMessage.author.send(userId);
    receivedMessage.react("✅");
}

function getChannelIdCommand(arguments, receivedMessage) {
    receivedMessage.author.send(receivedMessage.channel.id);
    receivedMessage.react("✅");
}

function earnMoneyPerMsg(receivedMessage, gain) {
    var selectQuery = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
    var sqlQuery = mySqlClient.query(selectQuery);
    sqlQuery.on("result", function(row) {
        if (row.balance >= gainPerMsg) {
            var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = '" + receivedMessage.author.id + "'";
            var sqlQuery2 = mySqlClient.query(selectQuery2);
            var total = row.balance - gainPerMsg;
            var updateQuery = "UPDATE `userdata` SET `balance` = '" + total + "' WHERE `userdata`.`id` = 'taverne'"
            mySqlClient.query(updateQuery);

            sqlQuery2.on("result", function(row) {
                var total2 = row.balance + gainPerMsg;
                var updateQuery2 = "UPDATE `userdata` SET `balance` = '" + total2 + "' WHERE `userdata`.`id` = '" + receivedMessage.author.id + "'";
                mySqlClient.query(updateQuery2);
                var updateQuery3 = "UPDATE `userdata` SET `lastUpdate` = '"+ receivedMessage.createdTimestamp + "' WHERE `userdata`.`id` = '" + receivedMessage.author.id + "'";
                mySqlClient.query(updateQuery3);
            });
        }
    });
}

function testBalance(arguments, receivedMessage, admin, moneyWhenCreatingAccount) {
    var inarg = arguments;
    var inrm = receivedMessage;

    if (arguments.length == 0) {
        var testQuery = "SELECT COUNT(*) AS test FROM `userdata` WHERE `id` = '" + receivedMessage.author.id + "'";
        var sqlQuery = mySqlClient.query(testQuery);
        receivedMessage.react("✅");
        var updateQuery = "UPDATE `userdata` SET `lastUpdate` = '"+ receivedMessage.createdTimestamp + "' WHERE `userdata`.`id` = '" + receivedMessage.author.id + "'";
        mySqlClient.query(updateQuery);

        sqlQuery.on("result", function(row) {
            if (row.test == 1) {
                balance(inarg, inrm);
            }
            else {
                create(inarg, inrm, moneyWhenCreatingAccount);
            }
        });
    } else if (arguments.length == 1 && admin.includes(receivedMessage.author.id)) {
        try {
            var userId = arguments.toString().split("@")[1].split(">")[0];
            userId = userId.startsWith("!") ? userId.substring(1) : userId;
            var testQuery = "SELECT COUNT(*) AS test FROM `userdata` WHERE `id` = '" + userId + "'";
            var sqlQuery = mySqlClient.query(testQuery);
            receivedMessage.react("✅");

            sqlQuery.on("result", function(row) {
              if (row.test == 1) {
                balance(inarg, inrm);
              }
              else {
                receivedMessage.author.send("Cet utilisateur n'a pas de compte bancaire.")
              }
          });
        } catch (error) {
            receivedMessage.author.send("Utilisateur inconnu.")
            receivedMessage.react("❌");
        }
    } else {
        receivedMessage.channel.send("Je ne suis pas sûr de ce que vous voulez. Essayez `t!aide`")
        receivedMessage.react("❌");
    }
}

// Fonction appelée lorsqu'un utilisateur fait appel à la fonction §solde mais n'a pas de compte dans la BDD.
function create(arguments, receivedMessage, moneyWhenCreatingAccount) {

    var selectQuery = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
    var sqlQuery = mySqlClient.query(selectQuery);

    sqlQuery.on("result", function(row) {
        if (parseFloat(row.balance) < parseFloat(moneyWhenCreatingAccount)) {
            var createQuery = "INSERT INTO `userdata` (`id`, `balance`, `lastUpdate`) VALUES ('" + receivedMessage.author.id + "', '0', '" + receivedMessage.createdTimestamp + "')";
            mySqlClient.query(createQuery);
            receivedMessage.author.send("Votre compte vient d'être crée, vous pouvez à partir de maintenant, accumuler de l'argent.\nVous avez actuellement 0§ dans votre compte.")
        } else {
            var createQuery = "INSERT INTO `userdata` (`id`, `balance`, `lastUpdate`) VALUES ('" + receivedMessage.author.id + "', '" + moneyWhenCreatingAccount + "', '" + receivedMessage.createdTimestamp + "')";
            var sqlQuery2 = mySqlClient.query(createQuery);

            sqlQuery2.on("result", function(row) {
                receivedMessage.author.send("Votre compte vient d'être crée, vous pouvez à partir de maintenant, accumuler de l'argent.\nVous avez actuellement " + moneyWhenCreatingAccount + "§ dans votre compte.")
                var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
                var sqlQuery2 = mySqlClient.query(selectQuery2);

                sqlQuery2.on("result", function(row) {
                    let total = parseFloat(row.balance) - parseFloat(moneyWhenCreatingAccount);
                    var updateQuery = "UPDATE `userdata` SET `balance` = '" + total + "' WHERE `userdata`.`id` = 'taverne';"
                    mySqlClient.query(updateQuery);
                });
            });
        }
    });
}

function balance(arguments, receivedMessage) {
    if (arguments.length == 0) {
      var testQuery = "SELECT * FROM `userdata` WHERE `id` = '" + receivedMessage.author.id + "'";
      var sqlQuery = mySqlClient.query(testQuery);

      sqlQuery.on("result", function(row) {
          receivedMessage.author.send("Votre solde est de : " + row.balance + "§.")
      });
    } else {
        var userId = arguments.toString().split("@")[1].split(">")[0];
        userId = userId.startsWith("!") ? userId.substring(1) : userId;
        var selectQuery = "SELECT * FROM `userdata` WHERE `id` = '" + userId + "'";
        var sqlQuery = mySqlClient.query(selectQuery);
        var indata = arguments;

        sqlQuery.on("result", function(row) {
          receivedMessage.author.send("Le solde de " + indata + " est de : " + row.balance + "§.")
        });
    }
}

function payCommand(arguments, receivedMessage) {
    if (arguments.length == 2) {
      try {
        // Utilisateur qui reçoit
        var userId = arguments[0].toString().split("@")[1].split(">")[0];
        userId = userId.startsWith("!") ? userId.substring(1) : userId;
        var selectQuery = "SELECT * FROM `userdata` WHERE `id` = '" + userId + "'";
        var sqlQuery = mySqlClient.query(selectQuery);
        var indata = arguments;

        if (!isNaN(arguments[1]) && arguments[1] > 0) {
          sqlQuery.on("result", async function(row) {
              // Utilisateur qui donne
              var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = '" + receivedMessage.author.id + "'";
              var sqlQuery2 = mySqlClient.query(selectQuery2);
              // Thune du gars qui reçoit
              var moneyReceiver = row.balance;
              receivedMessage.react("✅");

              sqlQuery2.on("result", function(row) {
                  var test = parseFloat(row.balance) - parseFloat(indata[1]);
                  if (test < 0) {
                      receivedMessage.channel.send("Vous ne possédez pas assez de Shams.")
                  }
                  else {
                      var total = parseFloat(moneyReceiver) + parseFloat(indata[1]);
                      var userId2 = indata[0].toString().split("@")[1].split(">")[0];
                      userId2 = userId2.startsWith("!") ? userId2.substring(1) : userId2;
                      var updateQuery = "UPDATE `userdata` SET `balance` = '" + total + "' WHERE `userdata`.`id` = '" + userId2 + "';"
                      mySqlClient.query(updateQuery);
                      removeBalance(receivedMessage, indata[1]);
                  }
                });
          });
        } else {
          receivedMessage.channel.send("Valeur invalide. Veuillez réessayez avec une bonne valeur.\n*Attention, utilisez un point et non une virgule pour les centimes.*")
          receivedMessage.react("❌");
        }
      } catch(error) {
        receivedMessage.channel.send("Destinataire invalide.\nt!payer [@Utilisateur] [Montant].")
        receivedMessage.react("❌");
      }
    } else {
      receivedMessage.channel.send("Il y a une erreur dans votre commande.\nt!payer [@Utilisateur] [Montant].")
      receivedMessage.react("❌");
    }
}

function removeBalance(rm, money) {
    var userId = rm.author.toString().split("@")[1].split(">")[0];
    userId = userId.startsWith("!") ? userId.substring(1) : userId;
    var selectQuery = "SELECT * FROM `userdata` WHERE `id` = '" + userId + "'";
    var sqlQuery = mySqlClient.query(selectQuery);

    sqlQuery.on("result", function(row) {
      var total = parseFloat(row.balance) - parseFloat(money);
      var userId2 = rm.author.toString().split("@")[1].split(">")[0];
      userId2 = userId2.startsWith("!") ? userId2.substring(1) : userId2;
      var updateQuery = "UPDATE `userdata` SET `balance` = '" + total + "' WHERE `userdata`.`id` = '" + userId2 + "';"
      mySqlClient.query(updateQuery);
      rm.author.send("Votre solde est désormais de " + total + "§.")
    });
}

function caisseCommand(arguments, receivedMessage) {
    if (arguments.length == 1) {
        var selectQuery = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
        var sqlQuery = mySqlClient.query(selectQuery);
        var indata = arguments;

        if (!isNaN(arguments) && arguments > 0) {
          sqlQuery.on("result", async function(row) {
              // Utilisateur qui donne
              var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = '" + receivedMessage.author.id + "'";
              var sqlQuery2 = mySqlClient.query(selectQuery2);
              var totaltaverne = row.balance;

              sqlQuery2.on("result", function(row) {
                  var test = parseFloat(row.balance) - parseFloat(indata);
                  if (test < 0) {
                      receivedMessage.author.send("Vous ne possédez pas assez de Shams.")
                      receivedMessage.react("❌");
                  }
                  else {
                      var total = parseFloat(totaltaverne) + parseFloat(indata);
                      var updateQuery = "UPDATE `userdata` SET `balance` = '" + total + "' WHERE `userdata`.`id` = 'taverne';"
                      mySqlClient.query(updateQuery);
                      removeBalance(receivedMessage, indata);
                      receivedMessage.react("✅");
                  }
                });
          });
        } else {
          receivedMessage.channel.send("Valeur invalide. Veuillez réessayez avec une bonne valeur.\n*Attention, utilisez un point et non une virgule pour les centimes.*")
          receivedMessage.react("❌");
        }
    } else {
      receivedMessage.channel.send("Il y a une erreur dans votre commande.\nt!caisse [Montant].")
      receivedMessage.react("❌");
    }
}

function bankCommand(arguments, receivedMessage) {
    if (arguments.length == 2) {
      try {
        // Utilisateur qui reçoit
        var userId = arguments[0].toString().split("@")[1].split(">")[0];
        userId = userId.startsWith("!") ? userId.substring(1) : userId;
        var selectQuery = "SELECT * FROM `userdata` WHERE `id` = '" + userId + "'";
        var sqlQuery = mySqlClient.query(selectQuery);
        var indata = arguments;

        if (!isNaN(arguments[1]) && arguments[1] > 0) {
          sqlQuery.on("result", async function(row) {
              // Utilisateur qui donne
              var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
              var sqlQuery2 = mySqlClient.query(selectQuery2);
              var userbalance = row.balance;

              sqlQuery2.on("result", function(row) {
                  var test = parseFloat(row.balance) - parseFloat(indata[1]);
                  if (test > 0) {
                      receivedMessage.react("✅");
                      var totaltaverne = parseFloat(row.balance) - parseFloat(indata[1]);
                      var updateQuery = "UPDATE `userdata` SET `balance` = '" + totaltaverne + "' WHERE `userdata`.`id` = 'taverne';"
                      mySqlClient.query(updateQuery);
                      var total = parseFloat(userbalance) + parseFloat(indata[1]);
                      var userId2 = indata[0].toString().split("@")[1].split(">")[0];
                      userId2 = userId2.startsWith("!") ? userId2.substring(1) : userId2;
                      var updateQuery2 = "UPDATE `userdata` SET `balance` = '" + total + "' WHERE `userdata`.`id` = '" + userId2 + "';"
                      mySqlClient.query(updateQuery2);
                      receivedMessage.author.send("Le solde de " + indata[0] + " est de : " + total + "§.")
                  } else {
                      receivedMessage.react("❌");
                      receivedMessage.channel.send("La valeur entrée est trop élevée, la banque ne possède pas autant de Shams.")
                  }
                });
          });
        } else {
          receivedMessage.channel.send("Valeur invalide. Veuillez réessayez avec une bonne valeur.\n*Attention, utilisez un point et non une virgule pour les centimes.*")
          receivedMessage.react("❌");
        }
      } catch(error) {
        receivedMessage.channel.send("Destinataire invalide.\nt!bank [@Utilisateur] [Montant].")
        receivedMessage.react("❌");
      }
    } else {
      receivedMessage.channel.send("Il y a une erreur dans votre commande.\nt!bank [@Utilisateur] [Montant].")
      receivedMessage.react("❌");
    }
}

function taxesCommand(arguments, receivedMessage) {
    if (arguments.length == 1) {
        var selectQuery = "SELECT * FROM `userdata` WHERE NOT `id` = 'taverne'";
        var sqlQuery = mySqlClient.query(selectQuery);
        receivedMessage.react("✅");
        var indata = arguments;
        var bank = 0;

        sqlQuery.on("result", function(row) {
            let calcul = parseFloat(row.balance) - (parseFloat(row.balance) * parseFloat(indata));
            bank = bank + parseFloat(row.balance) * parseFloat(indata);
            var updateQuery = "UPDATE `userdata` SET `balance` = '" + calcul + "' WHERE `userdata`.`id` = '" + row.id + "';"
            mySqlClient.query(updateQuery);

            var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
            var sqlQuery2 = mySqlClient.query(selectQuery2);

            sqlQuery2.on("result", function(row) {
                let calcul2 = parseFloat(row.balance) + parseFloat(bank);
                var updateQuery2 = "UPDATE `userdata` SET `balance` = '" + calcul2 + "' WHERE `userdata`.`id` = 'taverne';"
                mySqlClient.query(updateQuery2);
            });

        });
    }
    else {
        receivedMessage.channel.send("Il y a une erreur dans votre commande.\nt!taxes [Pourcentage (0.0 à 1.0)].")
        receivedMessage.react("❌");
    }
}

function hardReset(arguments, receivedMessage) {
    if (arguments.length == 1) {
        var deleteQuery = "DELETE FROM `userdata` WHERE NOT `id` = 'taverne';"
        mySqlClient.query(deleteQuery);
        var updateQuery = "UPDATE `userdata` SET `balance` = '" + arguments + "' WHERE `userdata`.`id` = 'taverne';"
        var sqlQuery = mySqlClient.query(updateQuery);
        var updateQuery2 = "UPDATE `userdata` SET `lastUpdate` = '0' WHERE `userdata`.`id` = 'taverne';"
        mySqlClient.query(updateQuery2);
        let indata = arguments;
        receivedMessage.react("✅");

        sqlQuery.on("result", function(row) {
            receivedMessage.channel.send("Hard reset effectué, la valeur maximale de l'économie du serveur a été définie à : " + indata + "§.")
        });
    } else {
        receivedMessage.channel.send("Il y a une erreur dans votre commande.\nt!hardreset [Valeur maximale de l'économie].")
        receivedMessage.react("❌");
    }
}

function setMaxEconomyValue(arguments, receivedMessage) {
    if (arguments.length == 1) {
        var selectQuery = "SELECT SUM(balance) AS `total` FROM `userdata`";
        var sqlQuery = mySqlClient.query(selectQuery);
        let indata = arguments;

        sqlQuery.on("result", function(row) {
            let diffToAddToTaverne = Math.abs(parseFloat(indata) - parseFloat(row.total))

            var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
            var sqlQuery2 = mySqlClient.query(selectQuery2);

            let a = parseFloat(indata); // La valeur max qu'on veut
            let b = parseFloat(row.total); // La valeur de la taverne actuel

            sqlQuery2.on("result", function(row) {
                if (a > b) {
                    let calcul = parseFloat(row.balance) + parseFloat(diffToAddToTaverne);
                    if (calcul < 0) {
                        receivedMessage.channel.send("La valeur maximale est trop basse par rapport à l'économie en circulation.")
                        receivedMessage.react("❌");
                    } else {
                        var updateQuery = "UPDATE `userdata` SET `balance` = '" + calcul + "' WHERE `userdata`.`id` = 'taverne';";
                        mySqlClient.query(updateQuery);
                        receivedMessage.channel.send("La valeur maximale de l'économie a été modifié à : " + indata + "§.")
                        receivedMessage.react("✅");
                    }
                } else {
                    let calcul = parseFloat(row.balance) - parseFloat(diffToAddToTaverne);
                    if (calcul < 0) {
                        receivedMessage.channel.send("La valeur maximale est trop basse par rapport à l'économie en circulation.")
                        receivedMessage.react("❌");
                    } else {
                        var updateQuery = "UPDATE `userdata` SET `balance` = '" + calcul + "' WHERE `userdata`.`id` = 'taverne';";
                        mySqlClient.query(updateQuery);
                        receivedMessage.channel.send("La valeur maximale de l'économie a été modifié à : " + indata + "§.")
                        receivedMessage.react("✅");
                    }
                }
            });
        });
    } else {
        receivedMessage.channel.send("Il y a une erreur dans votre commande.\nt!setmaxvalue [Valeur maximale de l'économie].")
        receivedMessage.react("❌");
    }
}

function checkOutdated(arguments, receivedMessage) {
    var selectQuery = "SELECT * FROM `userdata` WHERE NOT `id` = 'taverne'";
    var sqlQuery = mySqlClient.query(selectQuery);
    sqlQuery.on("result", function(row) {
        receivedMessage.react("✅");
        if ((receivedMessage.createdTimestamp - row.lastUpdate) > 1728000291) {
            var deleteQuery = "DELETE FROM `userdata` WHERE `id` = '" + row.id + "';"
            mySqlClient.query(deleteQuery);
            console.log(row.id + " deleted.")
            receivedMessage.author.send(row.id + " supprimé.")

            var moneyRemaining = row.balance;

            var selectQuery2 = "SELECT * FROM `userdata` WHERE `id` = 'taverne'";
            var sqlQuery2 = mySqlClient.query(selectQuery2);

            sqlQuery2.on("result", function(row) {
                let calcul = row.balance + moneyRemaining;
                var updateQuery = "UPDATE `userdata` SET `balance` = '" + calcul + "' WHERE `userdata`.`id` = 'taverne';";
                mySqlClient.query(updateQuery);
            });
        } else {
            console.log("All account are updated.")
            receivedMessage.author.send("Tous les comptes sont à jour.")
        }
    });
}


// ==================== MENU ==================== //

function menuCommand(arguments, receivedMessage) {
    // receivedMessage.channel.attachments.url("https://i.ibb.co/crW9756/Carte-SSS-La-Taverne.jpg");
    receivedMessage.channel.send("Et voici le menu de la taverne !", {
        file: "https://i.ibb.co/crW9756/Carte-SSS-La-Taverne.jpg"
    });
    receivedMessage.react("✅");
}

function menuHDCommand(arguments, receivedMessage) {
    receivedMessage.channel.send("Et voici le menu de la taverne en HD !", {
        file: "https://i.ibb.co/THSggXD/Carte-SSS-La-Taverne.png"
    });
    receivedMessage.react("✅");
}

// ---------- Wraps ---------- //

function menuWrapsClasssic(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/zVNY4Dc/Classsic.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!classsic [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuWrapsSSSpecial(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/nLjd1zR/SSSp-cial.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!ssspecial [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

// ---------- Burgers ---------- //

function menuBurgersCheessseburger(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/7GDHps8/Cheeseburger.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!cheessseburger [@Utilisateur] ou t!cburger [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuBurgersSSSBurger(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/h97JBpC/SSS-Burger.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!sssburger [@Utilisateur] ou t!sburger [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

// ---------- Plats ---------- //

function menuDishesPates(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/J3qmHpg/pates.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!pates [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

// ---------- Boissons ---------- //

function menuDrinkEau(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/6rPqGHk/Eau-Min-rale.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!eau [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuDrinkSSSCola(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/R69ppBM/SSS-Cola.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!ssscola [@Utilisateur] ou t!cola [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuDrinkIssseTea(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/m9f546q/Issse-Tea.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!isssetea [@Utilisateur] ou t!tea [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuDrinkSSSmoothie(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/qY7z34y/SSSmoothie.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!sssmoothie [@Utilisateur] ou t!smoothie [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuDrinkKanaille(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/zmmStNN/Kocktail-Kanaille.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!kanaille [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

// ---------- Bières ---------- //

function menuBeerDesperados(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/JCYGQZr/Desperados.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!desperados [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuBeerHeineken(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/xSH3J9L/Heineken.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!heineken [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuBeerAsahi(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/7WBJyC9/Asahi.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!asahi [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuBeerAffligem(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/8cV9LrK/Affligem.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!affligem [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuBeerGrimbergen(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/zbgSFs9/Grimbergen.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!grimbergen [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuBeerBrooklyn(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/KsZV4T8/Brooklyn.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!brooklyn [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

// ---------- Cafés ---------- //

function menuCoffeeCafe(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/Sy6NvW8/Caf.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!café [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuCoffeeGrandCafe(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/Q8Y44vh/Grand-Caf.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!grandcafé [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuCoffeeLatte(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/GHMcMR1/Latte.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!latte [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuCoffeeGrandLatte(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/5WZQs7m/Grand-Latte.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!grandlatte [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuCoffeeCappuccino(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/L17CbWM/Cappuccino.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!cappuccino [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuCoffeeCafeGourmand(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/VDZbZ8x/Caf-Gourmand.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!cafégourmand [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

// ---------- Desserts ---------- //

function menuDessertCookies(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/x37ZHt8/Cookies.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!cookies [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuDessertBananaSSSplit(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/2ygF8fF/Banana-SSSplit.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!bananasssplit [@Utilisateur] ou t!bs [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

function menuDessertTiramisssu(arguments, receivedMessage) {
    if (arguments.length == 1) {
        receivedMessage.channel.send("Et voici pour vous, " + arguments + " !", {
            file: "https://i.ibb.co/bXKbM6c/Tiramisssu.png"
        });
        receivedMessage.react("✅");
    } else {
        receivedMessage.channel.send("Erreur dans la commande.\nt!tiramisssu [@Utilisateur].");
        receivedMessage.react("❌");
    }
}

client.login(auth.token);
